package flow

import (
	"aff-internal-git-flow/shared"
	"flag"
	"fmt"
	"github.com/xanzy/go-gitlab"
)

func buildReviewers(reviewerID int) []int {
	if reviewerID == 0 {
		return []int{}
	}
	return []int{reviewerID}
}

func OnReleaseBranchCreate() {
	// retrieve required environment ci variables
	privateToken := shared.GetEnvOrExit("GITLAB_PRIVATE_TOKEN")
	projectID := shared.GetEnvIntOrExit("CI_PROJECT_ID")
	commitRefName := shared.GetEnvOrExit("CI_COMMIT_REF_NAME")
	authorID := shared.GetEnvIntOrExit("GITLAB_USER_ID")

	// retrieve options
	useTargetBranch := flag.String("target", "", "Target branch to merge into")
	useSourceBranch := flag.String("source", commitRefName, "Source branch default is the current branch")
	useReviewerID := flag.Int("reviewer", 5892650, "Reviewer ID to assign to the merge request, default is Lead Dev")
	flag.Parse()
	if useTargetBranch == nil || *useTargetBranch == "" {
		fmt.Println("No target branch specified")
		shared.EndFlow(shared.FAILURE)
	}

	// create a git client
	git, err := gitlab.NewClient(privateToken)
	if err != nil {
		panic(err)
	}

	// create merge requests
	_, response, mrError := git.MergeRequests.CreateMergeRequest(projectID, &gitlab.CreateMergeRequestOptions{
		SourceBranch: useSourceBranch,
		TargetBranch: useTargetBranch,
		Title:        gitlab.Ptr(fmt.Sprintf("Merge request from %s to main", commitRefName)),
		Description:  gitlab.Ptr("This merge request was created automatically by the release branch pipeline using aff-flow."),
		AssigneeID:   gitlab.Ptr(authorID),
		ReviewerIDs:  gitlab.Ptr(buildReviewers(*useReviewerID)),
	})

	if mrError != nil {
		if response.StatusCode == 409 {
			fmt.Printf("Merge request already exists to %s\n", *useTargetBranch)
		} else {
			fmt.Printf("Error creating merge request to %s: %v\n", *useTargetBranch, mrError)
			shared.EndFlow(shared.FAILURE)
		}
	} else {
		fmt.Printf("Merge request created to %s\n\n", *useTargetBranch)
	}

	fmt.Println("Finished flow on-release-branch-create")
	shared.EndFlow(shared.SUCCESS)
}
