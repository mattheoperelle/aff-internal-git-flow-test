FROM golang:1.21.11-alpine3.20 as build
WORKDIR /go/src/github.com/sdillen/my-awesome-app
WORKDIR /app
COPY . .
RUN CGO_ENABLED=0 go build -ldflags="-s -w" -o aff-flow /app/main.go


FROM alpine:3.20
RUN apk --no-cache add --no-check-certificate ca-certificates \
    && update-ca-certificates
COPY --from=build /app/aff-flow /usr/bin/aff-flow

ENTRYPOINT [""]
