package main

import (
	"aff-internal-git-flow/flow"
	"aff-internal-git-flow/shared"
	"fmt"
	"os"
)

func main() {
	if len(os.Args) < 2 {
		fmt.Println("No flow specified")
		shared.EndFlow(shared.FAILURE)
	}

	// remove the flow from the arguments for flow flags to be parsed correctly
	useFlow := os.Args[1]
	os.Args = os.Args[1:]

	switch useFlow {
	case "on-release-branch-create":
		flow.OnReleaseBranchCreate()
		break
	default:
		fmt.Println("No flow specified")
	}
}
