package shared

import (
	"fmt"
	"os"
	"strconv"
)

// GetEnvOrExit retrieves the value of an environment variable or exits the program if it is not set
func GetEnvOrExit(envName string) string {
	envValue := os.Getenv(envName)
	if envValue == "" {
		fmt.Printf("%s is not set\n", envName)
		os.Exit(1)
	}
	return envValue
}

// GetEnvOrDefault retrieves the value of an environment variable or returns a default value if it is not set
func GetEnvOr(envName string, defaultValue string) string {
	envValue := os.Getenv(envName)
	if envValue == "" {
		return defaultValue
	}
	return envValue

}

// GetEnvIntOrExit retrieves the value of an environment variable as an integer or exits the program if it is not set
func GetEnvIntOrExit(envName string) int {
	envValue := GetEnvOrExit(envName)
	envValueInt, err := strconv.Atoi(envValue)
	if err != nil {
		fmt.Printf("Error converting %s to int\n", envName)
		os.Exit(1)
	}
	return envValueInt
}

// GetEnvIntOrDefault retrieves the value of an environment variable as an integer or returns a default value if it is not set
func GetEnvIntOr(envName string, defaultValue int) int {
	envValue := os.Getenv(envName)
	if envValue == "" {
		return defaultValue
	}
	envValueInt, err := strconv.Atoi(envValue)
	if err != nil {
		fmt.Printf("Error converting %s to int\n", envName)
		os.Exit(1)
	}
	return envValueInt
}
