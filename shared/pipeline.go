package shared

import "os"

const (
	SUCCESS = iota
	FAILURE
	WARNING
)

func EndFlow(status int) {
	switch status {
	case SUCCESS:
		os.Exit(0)
	case FAILURE:
		os.Exit(1)
	case WARNING:
		os.Exit(2)
	}
}
